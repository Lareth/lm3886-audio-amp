#include <Arduino.h>

void fastBlink();

void setup() {
  fastBlink();
}

void loop() {
  fastBlink();
  

}

void fastBlink(){
  busy_wait_ms(500);
  digitalWrite(25, 1);
  busy_wait_ms(200);
  digitalWrite(25, 0);
}